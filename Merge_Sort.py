import sys, io

def sort(nodes):
    swaps = 0
    for i in range(1, len(nodes)):
        k = i
        while k > 0 and nodes[k] < nodes[k - 1]:
            swaps += 1
            tmp = nodes[k]
            nodes[k] = nodes[k - 1]
            nodes[k - 1] = tmp
            k -= 1
            #print(nodes)
    return nodes

def input_nodes():
    nodes = []
    while True:
        node = (yield)
        if node is None:
            yield sort(nodes)
        else:
            nodes.append(node)
            #print(nodes)

def merge_and_sort(arrays):
    merged_arr = [x for lists in arrays for x in lists]
    return sort(merged_arr)

def read_buffer(buffer1):
    arrays = []
    num_arrays = 2
    for n in range(num_arrays):
        in_nodes = input_nodes()
        next(in_nodes)
        entries = int(buffer1.readline())
        for n in buffer1.readline().split():
            in_nodes.send(int(n))
        arrays.append(in_nodes.send(None))
    #print(arrays[0])
    #print(arrays[1])
    return " ".join(str(e) for e in merge_and_sort(arrays))

if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            print(read_buffer(f))
    else:
        f = io.StringIO("4\n 2 4 10 18\n 3\n -5 11 12\n")
        result = read_buffer(f)
        print(result)
        if result == "-5 2 4 10 11 12 18":
            print("Test passed.")
        else:
            print("Test failed.")
