import sys
import collections


def print_degree(degree):
    if type(degree) is dict:
        sd = collections.OrderedDict(sorted((key, value) 
                                            for key, value in degree.items()))
        return ' '.join(str(degree[k]) for k in sd)
    else:
        raise Exception("wrong argument")


def degree_input(degree, from_node, to_node):
    if type(degree) is dict:
        for node in [from_node, to_node]:
            if node in degree:
                degree[node] += 1
            else:
                degree[node] = 1


def edge_input():
    degree = {}
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 2:
            degree_input(degree, int(edge[0]), int(edge[1]))
        else:
            yield print_degree(degree)


if __name__ == "__main__":
    in_edge = edge_input()
    next(in_edge)
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            f.readline()
            for line in f:
                in_edge.send(line.split())
        print(in_edge.send(None))
    else:
        edges = [['1', '2'], ['2', '4'], ['3', '6'], ['8', '6'], ['4', '3']]
        for edge in edges:
            in_edge.send(edge)
        result = in_edge.send(None)
        print(result)
        if result == '1 2 2 2 2 1':
            print('Test passed.')
        else:
            print('Test failed.')
