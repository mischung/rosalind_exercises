import sys


def get_degree(degree, node):
    return degree[node]


def print_sum(table, degree):
    if type(degree) is list and type(table) is list:
        sums = [0 for i in range(len(table))]
        for node in range(len(table)):
            for neighbour in get_neighbours(table, node):
                sums[node] += get_degree(degree, neighbour)
        return ' '.join(str(k) for k in sums)
    else:
        raise Exception("wrong argument")


def get_neighbours(table, node):
    result = []
    neighbours = table[node]
    for neighbour in range(len(neighbours)):
        if neighbours[neighbour] > 0:
            result.append(neighbour)
    return result


def graph_input(table, degree, from_node, to_node):
    if type(degree) is list and type(table) is list and from_node != to_node:
        for node in [from_node, to_node]:
            degree[node] += 1
        table[from_node][to_node] += 1
        table[to_node][from_node] += 1


def edge_input(number_of_nodes):
    degree = [0 for i in range(number_of_nodes)]
    table = [[0 for i in range(number_of_nodes)]
             for j in range(number_of_nodes)]
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 2:
            graph_input(table, degree, int(edge[0]) - 1, int(edge[1]) - 1)
        else:
            yield print_sum(table, degree)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            nodes, entries = f.readline().split()
            in_edge = edge_input(int(nodes))
            next(in_edge)
            for line in f:
                in_edge.send(line.split())
            print(in_edge.send(None))
    else:
        nodes = 5
        in_edge = edge_input(nodes)
        next(in_edge)
        edges = [['1', '2'], ['2', '3'], ['4', '3'], ['2', '4']]
        for edge in edges:
            in_edge.send(edge)
        result = in_edge.send(None)
        print(result)
        if result == '3 5 5 5 0':
            print('Test passed.')
        else:
            print('Test failed.')
