import sys

def bellman_ford(graph, from_node):
    dist = ['x' for x in range(len(graph))]
    dist[from_node] = 0
    decreasing = False
    for it in range(len(graph)):
       search_queue = [x for x in range(len(graph)) if dist[x] != 'x']
       decreasing = False
       for x in search_queue:
            for node in get_neighbours(graph, x):
                #print("To node " + str(node + 1))
                #print("Prev dist " + str(dist[node]))
                #print("Min node dist " + str(dist[x]))
                #print("Edge weight " + str(graph[x][node]))
                acc = dist[x] + graph[x][node]
                #print("Acc " + str(acc))
                if dist[node] == 'x' or dist[node] > acc:
                    dist[node] = acc
                    decreasing = True
            min_val = None
            for key in search_queue:
                if dist[key] != 'x' and (min_val is None or min_val > dist[key]):
                    min_val = dist[key]
    if decreasing == True:
        return -1
    else:
        return dist

def search_and_print(graph):
    if type(graph) is list:
        result = bellman_ford(graph, 0)
        return ' '.join(str(k) for k in result)
    else:
        raise Exception("wrong argument")

def get_neighbours(graph, node):
    result = []
    neighbours = graph[node]
    for neighbour in range(len(neighbours)):
        #print("Neighbour to " + str(node + 1) + " is " + str(neighbour + 1) + " weight " + str(neighbours[neighbour]))
        if neighbours[neighbour] is not None:
            result.append(neighbour)
    return result


def graph_input(graph, from_node, to_node, weight):
    if type(graph) is list and from_node != to_node:
        #print("Edge added from " + str(from_node + 1) + " to " + str(to_node + 1) + " with weight " + str(weight))
        graph[from_node][to_node] = weight


def edge_input(number_of_nodes):
    graph = [[None for i in range(number_of_nodes)]
             for j in range(number_of_nodes)]
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 3:
            graph_input(graph, int(edge[0]) - 1, int(edge[1]) - 1, int(edge[2]))
        else:
            #for i in range(len(graph)):
            #    print(' '.join(str(k) for k in graph[i]))
            yield search_and_print(graph)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            nodes, entries = f.readline().split()
            in_edge = edge_input(int(nodes))
            next(in_edge)
            for line in f:
                in_edge.send(line.split())
            print(in_edge.send(None))
    else:
        nodes = 9
        in_edge = edge_input(nodes)
        next(in_edge)
        edges = [[1, 2, 10],
                 [3, 2, 1],
                 [3, 4, 1],
                 [4, 5, 3],
                 [5, 6, -1],
                 [7, 6, -1],
                 [8, 7, 1],
                 [1, 8, 8],
                 [7, 2, -4],
                 [2, 6, 2],
                 [6, 3, -2],
                 [9, 5, -10],
                 [9, 4, 7]]
        for edge in edges:
            in_edge.send(edge)
        result = in_edge.send(None)
        print(result)
        if result == '0 5 5 6 9 7 9 8 x':
            print('Test passed.')
        else:
            print('Test failed.')
