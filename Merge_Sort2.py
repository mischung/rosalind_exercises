import sys, io

def sort(nodes, sort_from_index, sort_to_index, divider, increasing_order):
    #swaps = 0
    ordinal_factor = 1
    if increasing_order == True:
        ordinal_factor = 1
    else:
        ordinal_factor = -1
    for i in range(divider, sort_to_index  - sort_from_index):
        k = i
        while k > sort_from_index and nodes[k] * ordinal_factor < nodes[k - 1] * ordinal_factor:
            #swaps += 1
            tmp = nodes[k]
            nodes[k] = nodes[k - 1]
            nodes[k - 1] = tmp
            k -= 1
            #print(nodes)
    return nodes

def input_nodes():
    nodes = []
    while True:
        node = (yield)
        if node is None:
            yield nodes
        else:
            nodes.append(node)
            #print(nodes)

def merge_sort(array):
    #bottom up divide and conquer
    finished = False
    level = 1
    len_arr = len(array)
    #print("Length or list is " + str(len_arr))
    if len_arr == 1:
        return array
    while not finished:
        #print("Processing level " + str(level))
        chunk_size = 2 ** level
        for start_index in range(0, len_arr, chunk_size):
            stop_index = min(start_index + chunk_size, len_arr)
            divider = chunk_size // 2 + start_index - 1
            #print("Sorting index from " + str(start_index) + " to index " + str(stop_index) + ".")
            sort(array, start_index, stop_index, divider, True)
        if chunk_size >= len_arr:
            finished = True
        level += 1
    return array

def read_buffer(buffer1):
    in_nodes = input_nodes()
    next(in_nodes)
    entries = int(buffer1.readline())
    for n in buffer1.readline().split():
        in_nodes.send(int(n))
    nodes = in_nodes.send(None)
    return " ".join(str(e) for e in merge_sort(nodes))

if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            print(read_buffer(f))
    else:
        f = io.StringIO("10\n 20 19 35 -18 17 -20 20 1 4 4\n")
        result = read_buffer(f)
        print(result)
        if result == "-20 -18 1 4 4 17 19 20 20 35":
            print("Test passed.")
        else:
            print("Test failed.")
