import sys


def b_search(table, from_node, target_node, traversed, depth=0, max_depth=1000):
    result = None
    search_queue = [from_node]
    searched = []
    while result is None:
        next_queue = []
        if depth > max_depth:
            result = -2
        if len(search_queue) == 0:
            result = -1
        for search_node in search_queue:
            for node in get_neighbours(table, search_node):
                if node == target_node:
                    result = depth
                    break
                elif any(element for element in searched if node == element):
                    continue
                else:
                    next_queue.append(node)
                    searched.append(node)
        search_queue = next_queue
        searched.append(next_queue)
        depth += 1
    return result


def search_and_print(table):
    result = [0]
    if type(table) is list:
        for node in range(1, len(table)):
            result.append(b_search(table, 0, node, [0], 1))
        return ' '.join(str(k) for k in result)
    else:
        raise Exception("wrong argument")


def get_neighbours(table, node):
    result = []
    neighbours = table[node]
    for neighbour in range(len(neighbours)):
        if neighbours[neighbour] > 0:
            result.append(neighbour)
    return result


def graph_input(table, from_node, to_node):
    if type(table) is list and from_node != to_node:
        table[from_node][to_node] += 1


def edge_input(number_of_nodes):
    table = [[0 for i in range(number_of_nodes)]
             for j in range(number_of_nodes)]
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 2:
            graph_input(table, int(edge[0]) - 1, int(edge[1]) - 1)
        else:
            yield search_and_print(table)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            nodes, entries = f.readline().split()
            in_edge = edge_input(int(nodes))
            next(in_edge)
            for line in f:
                in_edge.send(line.split())
            print(in_edge.send(None))
    else:
        nodes = 6
        in_edge = edge_input(nodes)
        next(in_edge)
        edges = [['4', '6'], ['6', '5'], ['4', '3'], ['3', '5'],
                 ['2', '1'], ['1', '4']]
        for edge in edges:
            in_edge.send(edge)
        result = in_edge.send(None)
        print(result)
        if result == '0 -1 2 1 3 2':
            print('Test passed.')
        else:
            print('Test failed.')
