import sys


def bellman_ford(graph, from_node):
    dist = ['x' for x in range(len(graph))]
    dist[from_node] = 0
    decreasing = False
    for it in range(len(graph) + 1):
       search_queue = [x for x in range(len(graph)) if dist[x] != 'x']
       decreasing = False
       for x in search_queue:
            for node in get_neighbours(graph, x):
                #print("To node " + str(node + 1))
                #print("Prev dist " + str(dist[node]))
                #print("Edge weight " + str(graph[x][node]))
                acc = dist[x] + graph[x][node]
                #print("Acc " + str(acc))
                if dist[node] == 'x' or dist[node] > acc:
                    dist[node] = acc
                    decreasing = True
            min_val = None
            for key in search_queue:
                if dist[key] != 'x' and (min_val is None or min_val > dist[key]):
                    min_val = dist[key]
    if decreasing == True:
        return 1
    else:
        return -1

def search_and_print(graph):
    if type(graph) is list:
        result = bellman_ford(graph, 0)
        return str(result)
    else:
        raise Exception("wrong argument")

def get_neighbours(graph, node):
    result = []
    neighbours = graph[node]
    for neighbour in range(len(neighbours)):
        #print("Neighbour to " + str(node + 1) + " is " + str(neighbour + 1) + " weight " + str(neighbours[neighbour]))
        if neighbours[neighbour] is not None:
            result.append(neighbour)
    return result


def graph_input(graph, from_node, to_node, weight):
    if type(graph) is list and from_node != to_node:
        #print("Edge added from " + str(from_node + 1) + " to " + str(to_node + 1) + " with weight " + str(weight))
        graph[from_node][to_node] = weight


def edge_input(number_of_nodes):
    graph = [[None for i in range(number_of_nodes)]
             for j in range(number_of_nodes)]
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 3:
            graph_input(graph, int(edge[0]) - 1, int(edge[1]) - 1, int(edge[2]))
        else:
            #for i in range(len(graph)):
            #    print(' '.join(str(k) for k in graph[i]))
            yield search_and_print(graph)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            results = []
            graphs = f.readline()
            for graph in range(int(graphs)):
                nodes, entries = f.readline().split()
                in_edge = edge_input(int(nodes))
                next(in_edge)
                for i in range(int(entries)):
                    in_edge.send(f.readline().split())
                results.append(in_edge.send(None))
            print(' '.join(k for k in results))
    else:
        nodes = [4, 3]
        golden_results = [-1, 1]
        passed = True
        graphs = [[[1, 4, 4],
                 [4, 2, 3],
                 [2, 3, 1],
                 [3, 1, 6],
                 [2, 1, -7]],
                 [[1, 2, -8],
                 [2, 3, 20],
                 [3, 1, -1],
                 [3, 2, -30]]]
        for edges in graphs:
            in_edge = edge_input(nodes.pop(0))
            next(in_edge)
            for edge in edges:
                in_edge.send(edge)
            result = in_edge.send(None)
            print(result)
            if result != str(golden_results.pop(0)):
#                print('-')
                passed = False
#            else:
#                print('+')
        if passed == True:
            print("Test passed.")
        else:
            print ("Test failed.")
