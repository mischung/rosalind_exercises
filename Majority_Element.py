import sys, io
from Merge_Sort2 import merge_sort, sort
def majority(nodes):
    #print(nodes)
    sorted_nodes = merge_sort(nodes)
    #print(sorted_nodes)
    largest_group = [0, 0]
    current_group = [0, 0]
    for node in sorted_nodes:
        if node > current_group[0] or current_group[1] == 0:
            if current_group[1] > largest_group[1]:
                largest_group[0] = current_group[0]
                largest_group[1] = current_group[1]
            current_group[0] = node
            current_group[1] = 1
        else:
            current_group[1] += 1
    if current_group[1] > largest_group[1]:
        largest_group[0] = current_group[0]
        largest_group[1] = current_group[1]
    #print(largest_group)
    if largest_group[1] > len(nodes) // 2:
        return largest_group[0]
    else:
        return -1

def input_nodes():
    nodes = []
    while True:
        node = (yield)
        if node is None:
            yield nodes
        else:
            nodes.append(node)
            #print(nodes)

def read_buffer(buffer1):
    results = []
    entries, size_nodes = buffer1.readline().split()
    for entry in range(int(entries)):
        in_nodes = input_nodes()
        next(in_nodes)
        for node in buffer1.readline().split():
            in_nodes.send(int(node))
        results.append(majority(in_nodes.send(None)))
    return " ".join(str(e) for e in results)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            print(read_buffer(f))
    else:
        f = io.StringIO("4 8\n 5 5 5 5 5 5 5 5\n 8 7 7 7 1 7 3 7\n 7 1 6 5 10 100 1000 1\n 5 1 6 7 1 1 10 1\n")
        result = read_buffer(f)
        print(result)
        if result == "5 7 -1 -1":
            print("Test passed.")
        else:
            print("Test failed.")
