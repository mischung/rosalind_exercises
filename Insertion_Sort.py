import sys

def sort_and_print(nodes):
    swaps = 0
    for i in range(1, len(nodes)):
        k = i
        while k > 0 and nodes[k] < nodes[k - 1]:
            swaps += 1
            tmp = nodes[k]
            nodes[k] = nodes[k - 1]
            nodes[k - 1] = tmp
            k -= 1
            #print(nodes)
    return swaps

def input_nodes():
    nodes = []
    while True:
        node = (yield)
        if node is None:
            yield sort_and_print(nodes)
        else:
            nodes.append(node)
            #print(nodes)

if __name__ == "__main__":
    in_nodes = input_nodes()
    next(in_nodes)
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            entries = int(f.readline())
            for n in f.readline().split():
                in_nodes.send(int(n))
        print(in_nodes.send(None))
    else:
        line0 = "6"
        line1 = "6 10 4 5 1 2"
        entries = int(line0)
        for n in line1.split():
            in_nodes.send(int(n))
        result = in_nodes.send(None)
        print(result)
        if result == 12:
            print("Test passed.")
        else:
            print("Test failed.")
