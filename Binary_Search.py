import sys

def bin_find(arr, query):
    l_bound = 1
    u_bound = len(arr)
    query = int(query)
    while u_bound > l_bound:
        if query == int(arr[u_bound - 1]):
            return u_bound
        elif query == int(arr[l_bound - 1]):
            return l_bound
        diff = l_bound + (u_bound - l_bound) // 2
        if diff == l_bound:
            return -1
        elif query > int(arr[diff - 1]):
            l_bound = diff
        elif query < int(arr[diff - 1]):
            u_bound = diff
        else:
            return diff
    return -1

def batch(arr_len, query_len, arr, query):
    if len(arr) == arr_len and len(query) == query_len:
        ans = []
        for q in query:
            a = bin_find(arr, q)
            ans.append(a)
        return ans
    return []

if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], "r") as f:
            arr_len = int(f.readline())
            query_len = int(f.readline())
            arr = f.readline().split()
            query = f.readline().split()
        ans = batch(arr_len, query_len, arr, query)
        for a in ans:
            print(a, end=" ")
    else:
        arr_len = 5
        query_len = 5
        arr = ['10', '20', '30', '40', '50']
        query = ['1', '100', '30', '44', '20']
        golden = [-1, -1, 3, -1, 2]
        ans = batch(arr_len, query_len, arr, query)
        correct = 0
        while len(golden) > 0:
            if golden.pop() == ans.pop():
                correct = correct + 1
        if correct < query_len:
            print('test error')
        else:
            print('test passed')
