import sys


def dijkstra(graph, from_node):
    search_queue = [x for x in range(len(graph))]
    dist = [-1 for x in range(len(graph))]
    dist[from_node] = 0
    min_node = from_node
    search_queue.remove(from_node)
    while min_node is not None:
        for node in get_neighbours(graph, min_node):
            acc = dist[min_node] + graph[min_node][node]
            #print("To node " + str(node + 1))
            #print("Prev dist " + str(dist[node]))
            #print("Acc " + str(acc))
            if dist[node] < 0 or dist[node] > acc:
                dist[node] = acc
        min_node = None
        min_val = -1
        for key in search_queue:
            if dist[key] >=0 and (min_val < 0 or min_val > dist[key]):
                min_val = dist[key]
                min_node = key
        if min_node is not None:
            #print("Found min_node " + str(min_node) + " with distance " + str(min_val))
            search_queue.remove(min_node)
    return dist


def search_and_print(graph):
    if type(graph) is list:
        result = dijkstra(graph, 0)
        return ' '.join(str(k) for k in result)
    else:
        raise Exception("wrong argument")

def get_neighbours(graph, node):
    result = []
    neighbours = graph[node]
    for neighbour in range(len(neighbours)):
        if neighbours[neighbour] >= 0:
            result.append(neighbour)
    return result


def graph_input(graph, from_node, to_node, weight):
    if type(graph) is list and from_node != to_node:
        graph[from_node][to_node] = weight


def edge_input(number_of_nodes):
    graph = [[-1 for i in range(number_of_nodes)]
             for j in range(number_of_nodes)]
    while True:
        edge = (yield)
        if type(edge) is list and len(edge) == 3:
            graph_input(graph, int(edge[0]) - 1, int(edge[1]) - 1, int(edge[2]))
        else:
            #for i in range(len(graph)):
            #    print(' '.join(str(k) for k in graph[i]))
            yield search_and_print(graph)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            nodes, entries = f.readline().split()
            in_edge = edge_input(int(nodes))
            next(in_edge)
            for line in f:
                in_edge.send(line.split())
            print(in_edge.send(None))
    else:
        nodes = 6
        in_edge = edge_input(nodes)
        next(in_edge)
        edges = [[3, 4, 4],
                 [1, 2, 4],
                 [1, 3, 2],
                 [2, 3, 3],
                 [6, 3, 2],
                 [3, 5, 5],
                 [5, 4, 1],
                 [3, 2, 1],
                 [2, 4, 2],
                 [2, 5, 3]]
        for edge in edges:
            in_edge.send(edge)
        result = in_edge.send(None)
        print(result)
        if result == '0 3 2 5 6 -1':
            print('Test passed.')
        else:
            print('Test failed.')
