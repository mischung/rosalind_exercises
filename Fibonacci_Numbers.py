import sys

def fib(n):

    if n == 0:
        return int(0)
    elif n == 1:
        return int(1)
    elif n >= 2:
        return fib(n - 1) + fib(n - 2)
    else:
        print("n must be >= 0.")

if __name__ == "__main__":
    if len(sys.argv) == 2:
        print(sys.argv[0])
        print(fib(int(sys.argv[1])))
    else:
        exit()

