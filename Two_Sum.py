import sys, io

def two_sum(nodes):
    #print(nodes)
    result = "-1\n"
    for i in range(len(nodes)):
        for j in range(i + 1, len(nodes)):
            if nodes[i] == -nodes[j]:
                result = str(i + 1) + " " + str(j + 1) + "\n"
    return result

def input_nodes():
    nodes = []
    while True:
        node = (yield)
        if node is None:
            yield nodes
        else:
            nodes.append(node)
            #print(nodes)

def read_buffer(buffer1):
    results = []
    entries, size_nodes = buffer1.readline().split()
    for entry in range(int(entries)):
        in_nodes = input_nodes()
        next(in_nodes)
        for node in buffer1.readline().split():
            in_nodes.send(int(node))
        results.append(two_sum(in_nodes.send(None)))
    return " ".join(str(e) for e in results)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as f:
            print(read_buffer(f))
    else:
        f = io.StringIO("4 5\n 2 -3 4 10 5\n 8 2 4 -2 -8\n -5 2 3 2 -4\n 5 4 -5 6 8\n")
        result = read_buffer(f)
        print(result)
        if result == "-1\n 2 4\n -1\n 1 3\n":
            print("Test passed.")
        else:
            print("Test failed.")
